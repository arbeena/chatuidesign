import 'package:flutter/material.dart';
import 'package:whats_app_screen_list/model/chats_contents.dart';
import 'card_chat_details.dart';

class ChatScreenPage extends StatefulWidget {
  @override
  _ChatScreenPageState createState() => _ChatScreenPageState();
}

class _ChatScreenPageState extends State<ChatScreenPage> {
  List<ChatsContents> chatDetails = [
    ChatsContents(
        userName: 'User Teddy',
        chatsTitle: 'Hi, there!!',
        chatsImage:
            'https://121quotes.com/wp-content/uploads/2019/11/teddy-bear-images.jpg'),
    ChatsContents(
        userName: 'User Car',
        chatsTitle: 'Hello!! Whats up?',
        chatsImage:
            'https://www.extremetech.com/wp-content/uploads/2019/12/SONATA-hero-option1-764A5360-edit.jpg'),
    ChatsContents(
        userName: 'User Books',
        chatsTitle: 'Very interesting. Haha!!',
        chatsImage:
            'https://cdn.elearningindustry.com/wp-content/uploads/2016/05/top-10-books-every-college-student-read-1024x640.jpeg'),
    ChatsContents(
        userName: 'User Flower',
        chatsTitle: 'Amazing fragrance!!',
        chatsImage:
            'https://cdn.pixabay.com/photo/2015/04/19/08/33/flower-729512__340.jpg'),
    ChatsContents(
        userName: 'User Laptop',
        chatsTitle: 'Worderful!!',
        chatsImage:
            'https://cdn.vox-cdn.com/thumbor/e_QJsQqwaT63-qE_HaURX8sm9_A=/0x0:2040x1360/1200x675/filters:focal(887x732:1213x1058)/cdn.vox-cdn.com/uploads/chorus_image/image/65863020/akrales_161109_1261_A_0252.0.0.jpg'),
    ChatsContents(
        userName: 'User Teddy',
        chatsTitle: 'Hi, there!!',
        chatsImage:
            'https://121quotes.com/wp-content/uploads/2019/11/teddy-bear-images.jpg'),
    ChatsContents(
        userName: 'User Car',
        chatsTitle: 'Hello!! Whats up?',
        chatsImage:
            'https://www.extremetech.com/wp-content/uploads/2019/12/SONATA-hero-option1-764A5360-edit.jpg'),
    ChatsContents(
        userName: 'User Books',
        chatsTitle: 'Very interesting. Haha!!',
        chatsImage:
            'https://cdn.elearningindustry.com/wp-content/uploads/2016/05/top-10-books-every-college-student-read-1024x640.jpeg'),
    ChatsContents(
        userName: 'User Flower',
        chatsTitle: 'Amazing fragrance!!',
        chatsImage:
            'https://cdn.pixabay.com/photo/2015/04/19/08/33/flower-729512__340.jpg'),
    ChatsContents(
        userName: 'User Laptop',
        chatsTitle: 'Worderful!!',
        chatsImage:
            'https://cdn.vox-cdn.com/thumbor/e_QJsQqwaT63-qE_HaURX8sm9_A=/0x0:2040x1360/1200x675/filters:focal(887x732:1213x1058)/cdn.vox-cdn.com/uploads/chorus_image/image/65863020/akrales_161109_1261_A_0252.0.0.jpg'),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: chatDetails.length,
      itemBuilder: (context, index) {
        return CardWidget(
          chatDetails: chatDetails,
          index: index,
        );
      },
    );
  }
}
