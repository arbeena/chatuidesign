import 'package:flutter/material.dart';

class ChatsContents {
  final String userName;
  final String chatsTitle;
  String chatsImage;

  ChatsContents({@required this.userName, this.chatsTitle, this.chatsImage});
}
