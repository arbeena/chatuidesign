import 'package:flutter/material.dart';
import 'package:whats_app_screen_list/model/chats_contents.dart';

class CardWidget extends StatelessWidget {
  const CardWidget({@required this.chatDetails, this.index});

  final List<ChatsContents> chatDetails;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        onTap: () {
          showAlertDialog(context, chatDetails, index);
        },
        leading: CircleAvatar(
          backgroundImage: NetworkImage(chatDetails[index].chatsImage),
        ),
        title: Text(
          chatDetails[index].userName,
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.0,
            fontWeight: FontWeight.w500,
          ),
        ),
        subtitle: Text(
          chatDetails[index].chatsTitle,
          style: TextStyle(
            fontSize: 15.0,
            color: Colors.grey,
          ),
        ),
      ),
    );
  }
}

showAlertDialog(
    BuildContext context, List<ChatsContents> chatDetails, int index) {
  Widget oKButton = FlatButton(
    child: Text(
      'OK',
      style: TextStyle(
          color: Colors.teal.shade700,
          fontWeight: FontWeight.bold,
          fontSize: 16.0),
    ),
    onPressed: () {
      Navigator.of(context).pop(true);
    },
  );

  AlertDialog alert = AlertDialog(
    elevation: 15.0,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
    backgroundColor: Color(0xFFFFD4F5F0),
    title: Text('User Name: ' + chatDetails[index].userName),
    content: Text('Chat Title: ' + chatDetails[index].chatsTitle),
    actions: [
      oKButton,
    ],
  );

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
